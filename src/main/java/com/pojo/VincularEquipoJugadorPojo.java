package com.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VincularEquipoJugadorPojo {

	private int idEquipo;
	private int idJugador;
}
