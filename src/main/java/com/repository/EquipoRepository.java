package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.Equipo;

public interface EquipoRepository extends JpaRepository<Equipo, Long> {

}
