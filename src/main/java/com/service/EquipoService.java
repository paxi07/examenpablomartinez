package com.service;

import java.util.List;

import com.entity.Equipo;
import com.entity.Jugador;

public interface EquipoService {

	Equipo crearEquipo(Equipo equipo);

	List<Equipo> obtenerEquipos();

	Equipo obtenerEquipo(long id);

	List<Jugador> obtenerJugadoresMayoresEdad(long id, int edad);
}
