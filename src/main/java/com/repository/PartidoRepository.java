package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.Partido;

public interface PartidoRepository extends JpaRepository<Partido, Long> {

}
