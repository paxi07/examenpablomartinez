package com.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CrearPartidoPojo {

	private int idEquipoLocal;
	private int idEquipoVisitante;
	private String fecha;
	private int puntuacionEquipoLocal;
	private int puntuacionEquipoVisitante;

}
