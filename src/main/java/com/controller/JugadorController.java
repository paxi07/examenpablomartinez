package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Jugador;
import com.pojo.ContieneNombreJugadoresPojo;
import com.pojo.JugadorPojo;
import com.pojo.VincularEquipoJugadorPojo;
import com.service.EquipoService;
import com.service.JugadorService;

@RestController
@RequestMapping("/jugador")
public class JugadorController {

	@Autowired
	JugadorService jugadorService;

	@Autowired
	EquipoService equipoService;

	@PostMapping(value = "/addJugador")
	public Jugador crearJugador(@RequestBody final JugadorPojo jugadorPojo) {

		Jugador jugador = new Jugador();

		jugador.setApellidos(jugadorPojo.getApellidos());
		jugador.setNombre(jugadorPojo.getNombre());
		jugador.setEdad(jugadorPojo.getEdad());

		this.jugadorService.crearJugador(jugador);

		return jugador;
	}

	@GetMapping(value = "/getJugador/{id}")
	public Jugador buscarJugador(@PathVariable final long id) {

		return this.jugadorService.obtenerJugador(id);
	}

	@GetMapping(value = "/getJugadores")
	public List<Jugador> buscarJugador() {

		return this.jugadorService.obtenerJugadores();
	}

	@PutMapping(value = "/vincularEquipoJugador")
	public Jugador vincularEquipo(@RequestBody final VincularEquipoJugadorPojo vincularEquipoJugadorPojo) {

		int idEquipo = vincularEquipoJugadorPojo.getIdEquipo();

		int idJugador = vincularEquipoJugadorPojo.getIdJugador();

		return this.jugadorService.vincularEquipo(idEquipo, idJugador);
	}

	@GetMapping(value = "/getJugadoresFiltrados")
	public List<Jugador> buscarJugadoresFiltrados(
			@RequestBody final ContieneNombreJugadoresPojo contieneNombreJugadoresPojo) {

		return this.jugadorService.obtenerJugadoresFiltradoNombre(contieneNombreJugadoresPojo.getCadenaBusqueda());
	}
}
