package com.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EquipoPojo {

	private String nombre;
	private int fundacion;
}
