package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.EstadisticaJugador;

public interface EstadisticaJugadorRepository extends JpaRepository<EstadisticaJugador, Long> {

}
