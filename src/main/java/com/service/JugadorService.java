package com.service;

import java.util.List;

import com.entity.Jugador;

public interface JugadorService {

	Jugador crearJugador(Jugador jugador);

	List<Jugador> obtenerJugadores();

	Jugador obtenerJugador(long id);

	Jugador vincularEquipo(long idEquipo, long idJugador);

	List<Jugador> obtenerJugadoresFiltradoNombre(String cadenaBusqueda);
}
