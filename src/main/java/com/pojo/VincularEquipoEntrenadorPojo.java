package com.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VincularEquipoEntrenadorPojo {

	private int idEquipo;
	private int idEntrenador;
}
