package com.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Partido {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_equipoLocal")
	private Equipo equipoLocal;

	@ManyToOne
	@JoinColumn(name = "id_equipoVisitante")
	private Equipo equipoVisitante;

	private String fecha;
	private int puntuacionEquipoLocal;
	private int puntuacionEquipoVisitante;

	@OneToMany
	private List<EstadisticaJugador> estadisticaJugadoresLocal;

	@OneToMany
	private List<EstadisticaJugador> estadisticaJugadoresVisitante;
}
