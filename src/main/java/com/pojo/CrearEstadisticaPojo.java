package com.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CrearEstadisticaPojo {

	private int idPartido;
	private int idJugador;
	private int puntos;
	private int rebotes;
	private int asistencias;
}
