package com.service;

import com.entity.Partido;

public interface PartidoService {

	Partido crearPartido(Partido partido);

	Partido actualizarPuntuacion(long id, int putuacionLocal, int putuacionVisitante);

	Partido obtenerPartido(long id);
}
