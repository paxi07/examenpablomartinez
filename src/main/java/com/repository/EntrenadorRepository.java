package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.Entrenador;

public interface EntrenadorRepository extends JpaRepository<Entrenador, Long> {

}
