package com.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Equipo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String nombre;
	private int fundacion;

	@OneToMany
	private List<Jugador> jugadores;

	@OneToMany
	private List<Partido> partidosVisitante;

	@OneToMany
	private List<Partido> partidosLocal;

}
