package com.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ActualizarPuntuacionesPartidoPojo {

	private int idPartido;
	private int puntuacionEquipoLocal;
	private int puntuacionEquipoVisitante;
}
