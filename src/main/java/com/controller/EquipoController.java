package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Equipo;
import com.entity.Jugador;
import com.pojo.EquipoPojo;
import com.pojo.JugadoresMayorEdadPojo;
import com.service.EquipoService;

@RestController
@RequestMapping("/equipo")
public class EquipoController {

	@Autowired
	EquipoService equipoService;

	@PostMapping(value = "/addEquipo")
	public Equipo crearEquipo(@RequestBody final EquipoPojo equipoPojo) {

		Equipo equipo = new Equipo();

		equipo.setNombre(equipoPojo.getNombre());
		equipo.setFundacion(equipoPojo.getFundacion());

		this.equipoService.crearEquipo(equipo);

		return equipo;
	}

	@GetMapping(value = "/getEquipo/{id}")
	public Equipo buscarEquipo(@PathVariable final long id) {

		return this.equipoService.obtenerEquipo(id);
	}

	@GetMapping(value = "/getEquipos")
	public List<Equipo> buscarEquipos() {

		return this.equipoService.obtenerEquipos();
	}

	@GetMapping(value = "/getJugadoresEdad")
	public List<Jugador> obtenerJugadoresMayores(@RequestBody final JugadoresMayorEdadPojo jugadoresMayorEdadPojo) {

		int idEquipo = jugadoresMayorEdadPojo.getIdEquipo();
		int edad = jugadoresMayorEdadPojo.getEdad();

		return this.equipoService.obtenerJugadoresMayoresEdad(idEquipo, edad);
	}
}
