package com.service;

import java.util.List;

import com.entity.EstadisticaJugador;
import com.pojo.EstadisticasJugadorAgrupadasPojo;
import com.pojo.EstadisticasPartidoAgrupadasPojo;

public interface EstadisticaJugadorService {

	EstadisticaJugador crearEstadistica(EstadisticaJugador estadisticaJugador);

	List<EstadisticaJugador> obtenerEstadisticasJugador(long id);

	EstadisticasJugadorAgrupadasPojo obtenerEstadisticasJugadorAgrupadas(long id);

	EstadisticasPartidoAgrupadasPojo obtenerEstadisticasPartido(long idEquipoLocal, long idEquipoVisitante);
}
