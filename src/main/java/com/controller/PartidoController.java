package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Equipo;
import com.entity.Partido;
import com.pojo.ActualizarPuntuacionesPartidoPojo;
import com.pojo.CrearPartidoPojo;
import com.service.EquipoService;
import com.service.PartidoService;

@RestController
@RequestMapping("/partido")
public class PartidoController {

	@Autowired
	PartidoService partidoService;

	@Autowired
	EquipoService equipoService;

	@PostMapping(value = "/addPartido")
	public Partido crearPartido(@RequestBody final CrearPartidoPojo crearPartidoPojo) {

		Partido partido = new Partido();

		Equipo equipoLocal = equipoService.obtenerEquipo(crearPartidoPojo.getIdEquipoLocal());
		Equipo equipoVisitante = equipoService.obtenerEquipo(crearPartidoPojo.getIdEquipoVisitante());

		partido.setEquipoLocal(equipoLocal);
		partido.setEquipoVisitante(equipoVisitante);

		partido.setFecha(crearPartidoPojo.getFecha());
		partido.setPuntuacionEquipoLocal(crearPartidoPojo.getPuntuacionEquipoLocal());
		partido.setPuntuacionEquipoVisitante(crearPartidoPojo.getPuntuacionEquipoVisitante());

		this.partidoService.crearPartido(partido);

		return partido;
	}

	@PutMapping(value = "/updatePartido")
	public Partido updatePuntuaciones(
			@RequestBody final ActualizarPuntuacionesPartidoPojo actualizarPuntuacionesPartido) {

		int idPartido = actualizarPuntuacionesPartido.getIdPartido();
		int puntuacionLocal = actualizarPuntuacionesPartido.getPuntuacionEquipoLocal();
		int puntuacionVisitante = actualizarPuntuacionesPartido.getPuntuacionEquipoVisitante();

		return this.partidoService.actualizarPuntuacion(idPartido, puntuacionLocal, puntuacionVisitante);
	}
}
