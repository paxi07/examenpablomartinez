package com.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Equipo;
import com.entity.Jugador;
import com.repository.EquipoRepository;
import com.repository.JugadorRepository;
import com.service.JugadorService;

@Service
public class JugadorServiceImp implements JugadorService {

	@Autowired
	JugadorRepository jugadorRepository;

	@Autowired
	EquipoRepository equipoRepository;

	@Override
	public Jugador crearJugador(Jugador jugador) {

		this.jugadorRepository.save(jugador);

		return jugador;
	}

	@Override
	public List<Jugador> obtenerJugadores() {
		return this.jugadorRepository.findAll();
	}

	@Override
	public Jugador obtenerJugador(long id) {

		return this.jugadorRepository.findById(id).get();
	}

	@Override
	public Jugador vincularEquipo(long idEquipo, long idJugador) {

		Equipo equipo = equipoRepository.findById(idEquipo).get();
		Jugador jugador = jugadorRepository.findById(idJugador).get();

		jugador.setEquipo(equipo);

		this.jugadorRepository.save(jugador);

		return jugador;

	}

	@Override
	public List<Jugador> obtenerJugadoresFiltradoNombre(String cadenaBusqueda) {

		List<Jugador> jugadoresFiltrados = new ArrayList<Jugador>();

		for (Jugador jugador : this.jugadorRepository.findAll()) {
			if (jugador.getNombre().contains(cadenaBusqueda)) {
				jugadoresFiltrados.add(jugador);
			}
		}

		return jugadoresFiltrados;
	}
}
