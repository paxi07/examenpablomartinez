package com.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JugadorPojo {

	private String nombre;
	private String apellidos;
	private int edad;
}
