package com.serviceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Partido;
import com.repository.PartidoRepository;
import com.service.PartidoService;

@Service
public class PartidoServiceImp implements PartidoService {

	@Autowired
	PartidoRepository partidoRepository;

	@Override
	public Partido crearPartido(Partido partido) {

		return this.partidoRepository.save(partido);
	}

	@Override
	public Partido actualizarPuntuacion(long id, int putuacionLocal, int putuacionVisitante) {

		Partido partido = this.partidoRepository.findById(id).get();

		partido.setPuntuacionEquipoLocal(putuacionLocal);
		partido.setPuntuacionEquipoVisitante(putuacionVisitante);

		return this.partidoRepository.save(partido);
	}

	@Override
	public Partido obtenerPartido(long id) {

		return this.partidoRepository.findById(id).get();
	}

}
