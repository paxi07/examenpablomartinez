package com.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Equipo;
import com.entity.Jugador;
import com.repository.EquipoRepository;
import com.repository.JugadorRepository;
import com.service.EquipoService;

@Service
public class EquipoServiceImp implements EquipoService {

	@Autowired
	EquipoRepository equipoRepository;

	@Autowired
	JugadorRepository jugadorRepository;

	@Override
	public Equipo crearEquipo(Equipo equipo) {

		this.equipoRepository.save(equipo);

		return equipo;
	}

	@Override
	public List<Equipo> obtenerEquipos() {
		return this.equipoRepository.findAll();
	}

	@Override
	public Equipo obtenerEquipo(long id) {
		return this.equipoRepository.findById(id).get();
	}

	@Override
	public List<Jugador> obtenerJugadoresMayoresEdad(long id, int edad) {

		Equipo equipo = this.equipoRepository.findById(id).get();

		List<Jugador> jugadoresMayoresEdad = new ArrayList<Jugador>();

		for (Jugador jugador : this.jugadorRepository.findAll()) {
			if (jugador.getEquipo().equals(equipo) && jugador.getEdad() >= edad) {
				jugadoresMayoresEdad.add(jugador);
			}
		}

		return jugadoresMayoresEdad;
	}

}
