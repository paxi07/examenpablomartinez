package com.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.entity.Entrenador;
import com.entity.Equipo;
import com.repository.EntrenadorRepository;
import com.repository.EquipoRepository;
import com.service.EntrenadorService;

@Service
public class EntrenadorServiceImp implements EntrenadorService {

	@Autowired
	EntrenadorRepository entrenadorRepository;

	@Autowired
	EquipoRepository equipoRepository;

	@Override
	public Entrenador crearEntrenador(Entrenador entrenador) {

		this.entrenadorRepository.save(entrenador);

		return entrenador;
	}

	@Override
	public List<Entrenador> obtenerEntrenadores() {
		return this.entrenadorRepository.findAll();
	}

	@Override
	public Entrenador obtenerEntrenador(long id) {

		return this.entrenadorRepository.findById(id).get();
	}

	@Override
	public Entrenador vincularEquipo(long idEquipo, long idEntrenador) {

		Equipo equipo = equipoRepository.findById(idEquipo).get();
		Entrenador entrenador = entrenadorRepository.findById(idEntrenador).get();

		entrenador.setEquipo(equipo);

		this.entrenadorRepository.save(entrenador);

		return entrenador;
	}
}
