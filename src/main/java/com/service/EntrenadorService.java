package com.service;

import java.util.List;
import com.entity.Entrenador;

public interface EntrenadorService {

	Entrenador crearEntrenador(Entrenador entrenador);

	List<Entrenador> obtenerEntrenadores();

	Entrenador obtenerEntrenador(long id);

	Entrenador vincularEquipo(long idEquipo, long idEntrenador);

}
