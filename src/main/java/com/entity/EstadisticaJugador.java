package com.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class EstadisticaJugador {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private int puntos;
	private int rebotes;
	private int asistencias;

	@ManyToOne
	@JoinColumn(name = "id_partido")
	private Partido partido;

	@ManyToOne
	@JoinColumn(name = "id_jugador")
	private Jugador jugador;
}
