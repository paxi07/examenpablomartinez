package com.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ContieneNombreJugadoresPojo {

	private String cadenaBusqueda;
}
