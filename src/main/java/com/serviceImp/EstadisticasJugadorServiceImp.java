package com.serviceImp;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.entity.EstadisticaJugador;
import com.pojo.EstadisticasJugadorAgrupadasPojo;
import com.pojo.EstadisticasPartidoAgrupadasPojo;
import com.repository.EstadisticaJugadorRepository;
import com.repository.JugadorRepository;
import com.repository.PartidoRepository;
import com.service.EstadisticaJugadorService;

@Service
public class EstadisticasJugadorServiceImp implements EstadisticaJugadorService {

	@Autowired
	EstadisticaJugadorRepository estadisticaJugadorRepository;

	@Autowired
	JugadorRepository jugadorRepository;

	@Autowired
	PartidoRepository partidoRepository;

	@Override
	public EstadisticaJugador crearEstadistica(EstadisticaJugador estadisticaJugador) {

		return this.estadisticaJugadorRepository.save(estadisticaJugador);
	}

	@Override
	public List<EstadisticaJugador> obtenerEstadisticasJugador(long id) {

		List<EstadisticaJugador> estadisticasJugador = new ArrayList<EstadisticaJugador>();

		for (EstadisticaJugador estadisticaJugador : this.estadisticaJugadorRepository.findAll()) {
			if (estadisticaJugador.getJugador().getId().equals(id)) {
				estadisticasJugador.add(estadisticaJugador);
			}
		}

		return estadisticasJugador;
	}

	@Override
	public EstadisticasJugadorAgrupadasPojo obtenerEstadisticasJugadorAgrupadas(long id) {

		int totalPuntos = 0;
		int totalAsistencias = 0;
		int totalRebotes = 0;

		EstadisticasJugadorAgrupadasPojo estadisticasJugadorAgrupadas = new EstadisticasJugadorAgrupadasPojo();

		for (EstadisticaJugador estadisticaJugador : this.estadisticaJugadorRepository.findAll()) {
			if (estadisticaJugador.getJugador().getId().equals(id)) {
				totalPuntos += estadisticaJugador.getPuntos();
				totalAsistencias += estadisticaJugador.getAsistencias();
				totalRebotes += estadisticaJugador.getRebotes();
			}
		}

		estadisticasJugadorAgrupadas.setAsistenciasTotales(totalAsistencias);
		estadisticasJugadorAgrupadas.setPuntosTotales(totalRebotes);
		estadisticasJugadorAgrupadas.setRebotesTotales(totalPuntos);

		return estadisticasJugadorAgrupadas;
	}

	@Override
	public EstadisticasPartidoAgrupadasPojo obtenerEstadisticasPartido(long idEquipoLocal, long idEquipoVisitante) {

		return null;
	}
}
