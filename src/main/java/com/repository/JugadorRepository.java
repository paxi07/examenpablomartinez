package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.entity.Jugador;

public interface JugadorRepository extends JpaRepository<Jugador, Long> {

}
