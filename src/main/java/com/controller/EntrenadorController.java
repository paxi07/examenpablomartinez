package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.entity.Entrenador;
import com.pojo.EntrenadorPojo;
import com.pojo.VincularEquipoEntrenadorPojo;
import com.service.EntrenadorService;

@RestController
@RequestMapping("/entrenador")
public class EntrenadorController {

	@Autowired
	EntrenadorService entrenadorService;

	@PostMapping(value = "/addEntrenador")
	public Entrenador crearEntrenador(@RequestBody final EntrenadorPojo entrenadorPojo) {

		Entrenador entrenador = new Entrenador();

		entrenador.setApellidos(entrenadorPojo.getApellidos());
		entrenador.setNombre(entrenadorPojo.getNombre());
		entrenador.setEdad(entrenadorPojo.getEdad());

		this.entrenadorService.crearEntrenador(entrenador);

		return entrenador;
	}

	@GetMapping(value = "/getEntrenador/{id}")
	public Entrenador buscarEntrenador(@PathVariable final long id) {

		return this.entrenadorService.obtenerEntrenador(id);
	}

	@GetMapping(value = "/getEntrenadores")
	public List<Entrenador> buscarEntrenador() {

		return this.entrenadorService.obtenerEntrenadores();
	}

	@PutMapping(value = "/vincularEquipoEntrenador")
	public Entrenador vincularEquipo(@RequestBody final VincularEquipoEntrenadorPojo vincularEquipoEntrenadorPojo) {

		int idEquipo = vincularEquipoEntrenadorPojo.getIdEquipo();

		int idEntrenador = vincularEquipoEntrenadorPojo.getIdEntrenador();

		return this.entrenadorService.vincularEquipo(idEquipo, idEntrenador);
	}
}
