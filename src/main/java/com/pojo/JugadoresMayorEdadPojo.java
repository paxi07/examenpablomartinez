package com.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JugadoresMayorEdadPojo {

	private int idEquipo;
	private int edad;

}
