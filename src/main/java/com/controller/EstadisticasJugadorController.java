package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.entity.EstadisticaJugador;
import com.entity.Jugador;
import com.entity.Partido;
import com.pojo.CrearEstadisticaPojo;
import com.pojo.EstadisticasJugadorAgrupadasPojo;
import com.service.EstadisticaJugadorService;
import com.service.JugadorService;
import com.service.PartidoService;

@RestController
@RequestMapping("/estadistica")
public class EstadisticasJugadorController {

	@Autowired
	EstadisticaJugadorService estadisticaJugadorService;

	@Autowired
	JugadorService jugadorService;

	@Autowired
	PartidoService partidoService;

	@PostMapping(value = "/addEstadistica")
	public EstadisticaJugador crearPartido(@RequestBody final CrearEstadisticaPojo crearEstadisticaPojo) {

		EstadisticaJugador estadisticaJugador = new EstadisticaJugador();

		Jugador jugador = this.jugadorService.obtenerJugador(crearEstadisticaPojo.getIdJugador());
		Partido partido = this.partidoService.obtenerPartido(crearEstadisticaPojo.getIdPartido());

		estadisticaJugador.setJugador(jugador);
		estadisticaJugador.setPartido(partido);
		estadisticaJugador.setAsistencias(crearEstadisticaPojo.getAsistencias());
		estadisticaJugador.setPuntos(crearEstadisticaPojo.getPuntos());
		estadisticaJugador.setRebotes(crearEstadisticaPojo.getRebotes());

		this.estadisticaJugadorService.crearEstadistica(estadisticaJugador);

		return estadisticaJugador;
	}

	@GetMapping(value = "/getEstadisticasJugador/{id}")
	public List<EstadisticaJugador> obtenerEstadisticasJugador(@PathVariable final long id) {

		return estadisticaJugadorService.obtenerEstadisticasJugador(id);
	}

	@GetMapping(value = "/getEstadisticasJugadorAgrupadas/{id}")
	public EstadisticasJugadorAgrupadasPojo obtenerEstadisticasJugadorAgrupadas(@PathVariable final long id) {

		return estadisticaJugadorService.obtenerEstadisticasJugadorAgrupadas(id);
	}
}
